package com.nilhcem.hcsr04;

import android.arch.lifecycle.LifecycleActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;

public class MainActivity extends LifecycleActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String ECHO_PIN = "BCM25";
    private static final String TRIGGER_PIN = "BCM4";

    private Hcsr04 mHcsr04;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            mHcsr04 = new Hcsr04(ECHO_PIN, TRIGGER_PIN);
            mHcsr04.setOnDistanceReceivedListener(new Hcsr04.OnDistanceReceivedListener() {
                @Override
                public void onDistanceReceivedListener(double distanceInCm) {
                    Log.i(TAG, "OnDistanceReceived: " + distanceInCm);
                }
            });
        } catch (IOException e) {
            Log.e(TAG, "Error initializing HC-SR04", e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        for (int i = 0; i < 20; i++) {
            Log.i(TAG, "onResume: " + i);
            mHcsr04.getProximityDistance();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            mHcsr04.setOnDistanceReceivedListener(null);
            mHcsr04.close();
        } catch (Exception e) {
            Log.e(TAG, "Error closing HC-SR04", e);
        }
    }
}
