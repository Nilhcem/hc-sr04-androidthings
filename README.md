# HC-SR04 Ultrasonic Sensor Module Driver for Android Things

## Introduction
The HC-SR04 ultrasonic sensor uses sonar to determine distance to an object like bats or dolphins do.  
It offers excellent non-contact range detection with high accuracy and stable readings in an easy-to-use package. From 2cm to 400 cm or 1” to 13 feet.

## Schematic

![schematic.png][]

We use a voltage divider circuit using appropriate resistors to bring down the ECHO output Voltage (5V) to 3.3V (for the Raspberry Pi 3).

![schematic-volts.png][]

The following equation can be used for calculating resistor values:  
```
Vout = Vin * R2 / (R1 + R2)
3.3 = 5V * 2 / (1 + 2)
```
