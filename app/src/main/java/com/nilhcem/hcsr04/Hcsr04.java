package com.nilhcem.hcsr04;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.GpioCallback;
import com.google.android.things.pio.PeripheralManagerService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Hcsr04 implements AutoCloseable {

    private static final String TAG = Hcsr04.class.getSimpleName();

    public interface OnDistanceReceivedListener {

        void onDistanceReceivedListener(double distanceInCm);
    }

    private Gpio mEcho;
    private Gpio mTrigger;
    private OnDistanceReceivedListener mListener;

    private Handler mEchoHandler;
    private HandlerThread mEchoHandlerThread;

    private GpioCallback mEchoCallback = new GpioCallback() {

        private long mStartTime;

        @Override
        public boolean onGpioEdge(Gpio gpio) {
            try {
                long currentTime = System.nanoTime();

                if (gpio.getValue()) {
                    mStartTime = currentTime;
                } else {
                    double distanceInCm = ((double) TimeUnit.NANOSECONDS.toMicros(currentTime - mStartTime)) / 58; // or 148 for the distance in inches

                    OnDistanceReceivedListener listener = mListener;
                    if (listener != null) {
                        listener.onDistanceReceivedListener(distanceInCm);
                    }
                }
            } catch (IOException e) {
                Log.e(TAG, "onGpioEdge error", e);
            }

            return true;
        }

        @Override
        public void onGpioError(Gpio gpio, int error) {
            Log.e(TAG, "onGpioError: " + error);
        }
    };

    public Hcsr04(String echoPin, String triggerPin) throws IOException {
        mEchoHandlerThread = new HandlerThread("Hcsr04Thread");
        mEchoHandlerThread.start();
        mEchoHandler = new Handler(mEchoHandlerThread.getLooper());

        PeripheralManagerService pioService = new PeripheralManagerService();

        mEcho = pioService.openGpio(echoPin);
        mEcho.setDirection(Gpio.DIRECTION_IN);
        mEcho.setEdgeTriggerType(Gpio.EDGE_BOTH);
        mEcho.setActiveType(Gpio.ACTIVE_HIGH);
        mEcho.registerGpioCallback(mEchoCallback, mEchoHandler);

        mTrigger = pioService.openGpio(triggerPin);
        mTrigger.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
        mTrigger.setValue(false);
    }

    @Override
    public void close() throws IOException {
        try {
            mEcho.unregisterGpioCallback(mEchoCallback);
            mEchoHandlerThread.quitSafely();
            mEcho.close();
            mTrigger.close();
        } finally {
            mEchoHandler = null;
            mEchoHandlerThread = null;
        }
    }

    public void setOnDistanceReceivedListener(OnDistanceReceivedListener listener) {
        mListener = listener;
    }

    public void getProximityDistance() {
        if (mListener == null) {
            throw new RuntimeException("You must set an OnDistanceReceivedListener first");
        }

        try {
            // To start measurement, trigger pin must receive a pulse of high for at least 10us
            mTrigger.setValue(true);
            Thread.sleep(0, 10_000);
            mTrigger.setValue(false);
        } catch (IOException | InterruptedException e) {
            Log.e(TAG, "Error getting proximity distance", e);
        }
    }
}
